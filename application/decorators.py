import webapp2
import cgi
from queries import *

from google.appengine.api import users

"""Checks if the user is logged in.
	If yes, checks whether we have a user account for them, if yes proceed. If there is no user account, continue to the account manager link
	If the user is not logged in, redir to the account manager
"""
def login_required(func):
	def decorated_view(self, *args, **kwargs):
		#Check if the user is logged in
		user = users.get_current_user()
		if user == None:
			#No, send to login page
			return self.redirect(users.create_login_url( webapp2.uri_for("account_manager") + "?continue=" + cgi.escape(self.request.uri)))
		(key, cattle) = get_user_data_by_user_id(user.user_id())
		if key == None:
			return self.redirect(users.create_login_url( webapp2.uri_for("account_manager") + "?continue=" + cgi.escape(self.request.uri)))

		return func(self, *args, **kwargs)
	return decorated_view
