from google.appengine.api import mail

'''
Note: Created a general email address that would send the notification. 
*** Need to grant udder.appspot@gmail.com developoer access to app ***
'''
def send_mail_to_follower(followee_name, follower_email):
	message_body = followee_name + ' has posted a Teet!'
	# (sender had to be a Google Account that has
	# been added as a developer for the app.)
	message = mail.EmailMessage(
	sender='Steve Low <steve.a.low@gmail.com>', #Change to udder.appspot@gmail.com later
	to=follower_email,
	subject='New Teet From ' + followee_name + '!',
	body=message_body)

	message.send()

def send_email_notification(cattle):	
	if cattle.teet_count == 5:
		cattle.teet_count = 0
		cattle.put()
						
	elif cattle.teet_count == 0:
		for cf in cattle.followers:
			send_mail_to_follower(cattle.user_name, cf.follower.email)
			
		cattle.teet_count += 1
		cattle.put()
	else:
		cattle.teet_count += 1
		cattle.put()

def reset_teet_count(cattle):
	cattle.teet_count = 0
	cattle.put()
