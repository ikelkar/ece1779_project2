import logging

#!/usr/bin/env python
import webapp2

#Jinja2 - Template engine
import jinja2 
import json
import os

#Google services
from google.appengine.api import users
from google.appengine.ext import db

#Udder files
import models
from decorators import *
import memcachemodule
from queries import *
import mail_notification

import re
import datetime
import time

### TEMPLATING ###
jinja_environment = jinja2.Environment(loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

def render_template(self, template_name_string, params=None):
	print "rendering template " + template_name_string
	template = jinja_environment.get_template('templates/' + template_name_string)
	if params != None:
		self.response.out.write(template.render(params))
	else:
		self.response.out.write(template.render())
		
def render_to_json(self, params):
	data = json.dumps(params)
	self.response.out.write( data )

### /TEMPLATING ###

### IK NOTE - THESE NEED TO BE DELETED ###
### DB QUERY HELPERS ###

def get_latest_of_own_teets(cattle_key):
	msg_query = None
	msg_query = db.GqlQuery("SELECT * FROM Teet WHERE cattle = :1 ORDER BY message_time DESC LIMIT 1", cattle_key)
	
	teets = msg_query.fetch(1)
	for teet in teets:
		return teet

def get_some_teets():
	#Get list of all teets, print them to page
	teets_query = models.Teet.all()
	teets = teets_query.fetch(10)

	#Create a tuple of (username, teet) for printing
	teets_to_print = []

	for teet in teets:
		teets_to_print.append( ( teet.cattle.user_name, teet.text ) )
	
	return teets_to_print

### /DB QUERY HELPERS ###

### PUBLIC PAGE CONTROLLERS ###
class Home(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()
		
		# TODO show teets from people I'm following
		# look @ TeetsIndex for code

		if user:
			(key, cattle) = get_user_data_by_user_id(user.user_id())
			#slow: reset Teet count for email notification (disable for now)
			#mail_notification.reset_teet_count(cattle)

			print "key = " + str(key)
			if key != None:
				params = {	'teets_to_print': get_batch_of_own_teets(key, "DESC", 0),
								'logout_url': users.create_logout_url(self.url_for("home"))
							}
				render_template(self, "index_logged_in.html", params)
			else:
				render_template(self, "index_not_logged_in.html")
		else:
			render_template(self, "index_not_logged_in.html")
			
class TeetsFrontPageAjax(webapp2.RequestHandler):
	def post(self):
		user = users.get_current_user()

		if user:
			(key, cattle) = get_user_data_by_user_id(user.user_id())
			if key != None:
				teets_to_print = []
				timestamp = self.request.get('timestamp')
				if timestamp == "":
					timestamp = 0

				for teet in get_batch_of_own_teets(key, "DESC", timestamp):
					teets_to_print.append( ( teet.cattle.user_name, teet.text, int( time.mktime(teet.message_time.utctimetuple()) ) ) )

				params = { 
						'status' : 'success',
						'username' : cattle.user_name,
						'teets': teets_to_print
						}

				render_to_json(self, params)
		else:
			render_to_json(self, {'status': 'fail'})
		
class TeetsFollowAjax(webapp2.RequestHandler):
	def post(self):
		user = users.get_current_user()
		
		# show teets from people I'm following
		if user:
			#Create a tuple of (username, teet) for printing
			teets_to_print = []

			(key, cattle) = get_user_data_by_user_id(user.user_id())

			if key != None:
				timestamp = self.request.get('timestamp')
				if timestamp == "":
					timestamp = 0
				for teet in get_batch_of_following_teets(key, "DESC", timestamp):
					teets_to_print.append( ( teet.cattle.user_name, teet.text, int( time.mktime(teet.message_time.utctimetuple()) ) ) )
	
				params = { 	'status' : 'success',
								'teets': teets_to_print }
				render_to_json(self, params)
		else:
			render_to_json(self, {'status': 'fail'})
		
class TeetsGossipAjax(webapp2.RequestHandler):
	def post(self):
		user = users.get_current_user()
		
		# show teets from people I'm following
		if user:
			#Create a tuple of (username, teet) for printing
			teets_to_print = []

			(key, cattle) = get_user_data_by_user_id(user.user_id())

			if key != None:
				timestamp = self.request.get('timestamp')
				if timestamp == "":
					timestamp = 0

				loading_extra_teets = False

				try:
					number_loaded_teets = int(self.request.get('number_loaded_teets'))
					if number_loaded_teets == 0:
						timestamp = 0
						raise ValueError
					print "recieved num: " + str(number_loaded_teets)
					loading_extra_teets = True
					number_loaded_teets -= 1
				except ValueError:
					number_loaded_teets = 5
									
				print "number_loaded_teets: " + str(number_loaded_teets)
				print timestamp
				teets = get_batch_of_directed_teets(key, "DESC", timestamp, number_loaded_teets)
				print "regular"
				print [t.text for t in teets]

				if loading_extra_teets:
					extra_teets = get_upto_top_directed_teets(key, timestamp)
					print "extra"
					print [t.text for t in extra_teets]
					teets = extra_teets + teets

				for teet in teets:
					teets_to_print.append( ( teet.cattle.user_name, teet.text, int( time.mktime(teet.message_time.utctimetuple())) ) )
	
				params = { 	'status' : 'success',
								'teets': teets_to_print }
				render_to_json(self, params)
		else:
			render_to_json(self, {'status': 'fail'})
			
class TeetsBrandsAjax(webapp2.RequestHandler):
	def post(self):
		brand_name = cgi.escape(self.request.get('brand'))
		brand_name = brand_name.strip()
		
		(key, brand) = get_brand_by_name(brand_name)
		if key == None:
			render_to_json(self, {'status': 'fail'})
		else:
			teets_to_print = []
			for teet in brand.messages:
				teets_to_print.append( ( teet.cattle.user_name, teet.text ) )
				
			params = { 	'status' : 'success', 'teets': teets_to_print }
			print params
			render_to_json(self, params)

class StyleShowHandler(webapp2.RequestHandler):
	def get(self):
		render_template(self, "style.html")

class JquerySampleHandler(webapp2.RequestHandler):
	def get(self):
		render_template(self, "jqueryui.html")

class Login(webapp2.RequestHandler):
	def get(self):
		user = users.get_current_user()

		if user:
			(key, cattle) = get_user_data_by_user_id(user.user_id())

			if key == None:
				self.redirect(users.create_login_url( webapp2.uri_for("account_manager") + "?continue=" + self.url_for("home")))
			else:
				#Already logged in, redir away
				self.redirect_to("home")
		else:
			self.redirect(users.create_login_url( webapp2.uri_for("account_manager") + "?continue=" + self.url_for("home")))

class Logout(webapp2.RequestHandler):
	def get(self):
		self.redirect(users.create_logout_url(self.url_for("home")))

class UserProfile(webapp2.RequestHandler):
    """
    Get the user information based on user_id. 
    Performs query in Cattle db 
    """
    # get is for unit Testing purpose
    #@login_required
    #def get(self):
    #    params = { 'user_name' : "",
    #                'email' : "" }
    #    render_template(self, "test_template.html", params)
             
    @login_required
    def post(self): 
        user_name = self.request.get('user_name') 
        #logging.info(user_name)
        if not user_name is None: 
            (key, cattle) = get_user_data_by_user_name(str(user_name))
            cattle_instance = models.Cattle.get(key)
            #logging.info(cattle_instance.email)
            #logging.info(cattle_instance.user_name)
            #TODO: Add more info to return
            params = { 'status' : 'success',
                       'user_name' : cattle_instance.user_name,
                       'email' : cattle_instance.email,
                       'info' : cattle_instance.user_info }
            #render_template(self, "test_template.html", params)  #Testing
            #TODO: render to pop-up page
            render_to_json(self, params)
            
class SearchUser(webapp2.RequestHandler):
    """
    Get the user information based on user_id. 
    Performs query in Cattle db 
    """
    # get is for unit Testing purpose
    #@login_required
    #def get(self):
    #    params = { 'user_name' : "",
    #                'email' : "" }
    #    render_template(self, "test_template.html", params)
             
    @login_required
    def post(self): 
		partial_user_name= cgi.escape(self.request.get('term'))
		msg_query = db.GqlQuery("SELECT * FROM Cattle WHERE user_name >= :1 AND user_name < :2 ORDER BY user_name ASC", partial_user_name, unicode(partial_user_name) + u"\ufffd")
		cattle_list = msg_query.fetch(5)
		matching_cattle_list = [c.user_name for c in cattle_list]
		#params = { 'status': 'success', 'cattle_list': matching_cattle_list}
		params = matching_cattle_list
		render_to_json(self, params)
		
class SearchBrand(webapp2.RequestHandler):
    """
    Get the user information based on user_id. 
    Performs query in Cattle db 
    """
    # get is for unit Testing purpose
    #@login_required
    #def get(self):
    #    params = { 'user_name' : "",
    #                'email' : "" }
    #    render_template(self, "test_template.html", params)
             
    @login_required
    def post(self): 
		partial_brand = cgi.escape(self.request.get('partial_brand'))
		msg_query = db.GqlQuery("SELECT * FROM Brand WHERE name >= :1 AND name < :2 ORDER BY name ASC", partial_brand, unicode(partial_brand) + u"\ufffd")
		brands_list = msg_query.fetch(5)
		matching_brands_list = [b.name for b in brands_list]
		params = { 'status': 'success', 'brands_list': matching_brands_list}
		render_to_json(self, params)

class SaveUserProfile(webapp2.RequestHandler):
    @login_required
    def post(self): 
        new_info = self.request.get('info') 
        user = users.get_current_user()
        if not user is None: 
            (key, cattle) = get_user_data_by_user_id(user.user_id())
            cattle_instance = models.Cattle.get(key)
            #logging.info(new_info)
            cattle_instance.user_info = new_info;
            cattle_instance.put()
            memcachemodule.cache_delete(user.user_id())

### /PUBLIC PAGE CONTROLLERS ###

### TEET PAGES CONTROLLERS ###

class TeetsNew(webapp2.RequestHandler):

	def get_or_create_brand(self, brand_name):
		(key, brand) = get_brand_by_name(brand_name)
		if key == None:
			#Doesn't exist, Create the brand
			brand = models.Brand(name = brand_name)
			brand.put()
			key = brand.key()
		return key

	def parse_teet_for_tags(self, text):
		brands = []
		user_tags = []
		tokens = text.split()
		for token in tokens:
			if re.match(r"^#\w+", token):
				#Found a tag
				token = token[1:]
				token_key = TeetsNew.get_or_create_brand(self, token)
				if token_key != None:
					brands.append(token_key)
			elif re.match(r"^@.*", token):
				print "found @"
				token = token[1:]
				print token
				(user_key, user) = get_user_data_by_user_name(token)
				if user_key != None:
					user_tags.append(user_key)

		return (brands, user_tags)

	def store_teet(self, cattle, text, brands_list, user_tags):
		print "user tags"
		print user_tags
		teet = models.Teet(text = cgi.escape(text), brands = brands_list, user_refs=user_tags, cattle=cattle)
		teet.put()
		#slow: Need to delete key in cache and re-populate cache on the next query
		memcachemodule.cache_delete(cattle.key())
		return teet.message_time

	@login_required
	def post(self):
		text = self.request.get('teet-value')
		text = text.strip()

		user = users.get_current_user()
		(key, cattle) = get_user_data_by_user_id(user.user_id())
		
		# TODO
		# mmakowsk:	why does this fail here? (always for me)
		if key == None or text == "":
			#Uh oh, looks like a new teet was submitted by a user that does not have an entry in the DB?!
			#The only way I can see this happening is if the DataStore is manually purged of Cattle Entries
			#redir away and pretend this never happened...
			self.redirect_to("home")
			params = { 'result': 'fail' }
			render_to_json(self, params)
		else:
			(brands, user_tags) = TeetsNew.parse_teet_for_tags(self, text)
			timestamp = TeetsNew.store_teet(self, cattle, text, brands, user_tags)
			
			
			# mmakowsk:
			teets_to_print = []
			teets_to_print.append((cattle.user_name, text, int( time.mktime(timestamp.utctimetuple()))))
			
			#slow: Add Mail Notification
			if cattle != None:
				mail_notification.send_email_notification(cattle)
					
			params = { 'status': 'success', 'teets': teets_to_print }
			render_to_json(self, params)

class TeetEdit(webapp2.RequestHandler):
	@login_required
	def get(self, teetid):
		# For testing purpose
		# Will be replaced by a front end call
		teet_key = db.Key.from_path('Teet', int(teetid))
		teet_instance = models.Teet.get(teet_key)
		self.response.out.write(teet_instance.text)
		self.response.out.write("""
			<form action="/teets/edit/%s" method="post">
				New Teet: <input value="%s" name="new_teet_text">
				<input type="submit" value="Edit">
			</form>
		</body>
		</html>""" % (teetid, teet_instance.text))# % teetid)

	@login_required
	def post(self, teetid):
		teet_key = db.Key.from_path('Teet', int(teetid))
		teet_instance = models.Teet.get(teet_key)
		new_teet = cgi.escape(self.request.get('new_teet_text'))
		teet_instance.text = new_teet
		assert teet_instance.put()
		self.redirect("/")

class TeetBrand(webapp2.RequestHandler):
	@login_required
	def get(self, brand_name):
		#Find if the brand exists
		brand_name = brand_name.strip()
		(key, brand) = get_brand_by_name(brand_name)
		if key == None:
			#Brand doesn't exist - fail
			self.redirect("fuck_brand_fetch_failed")
		else:
			#Create a tuple of (username, teet) for printing
			teets_to_print = []
			for teet in brand.messages:
				teets_to_print.append( ( teet.cattle.user_name, teet.text ) )

			params = {	'teets_to_print': teets_to_print,
							'logout_url': users.create_logout_url(self.url_for("home"))
						}
			render_template(self, "brands.html", params)

### /TEET PAGES CONTROLLERS ###

### FOLLOW/UNFOLLOW PAGE CONTROLLERS ###
class Follow(webapp2.RequestHandler):

	### HELPER METHODS ###
	def follow_user(self, follower, followee):
		#Helper method to add follower/followee relationship
		cf = models.CattleFollower(followee = followee, follower = follower)
		cf.put()

	### /HELPER METHODS ###

	"""@login_required
	def get(self):
		user = users.get_current_user()
		(key, cattle) = get_user_data_by_user_id(user.user_id())
	
		if cattle != None:
			followers = []
			following = []
			for cf in cattle.following:
				following.append(cf.followee.user_name)
			for cf in cattle.followers:
				followers.append(cf.follower.user_name)
			params = {	'following': following,
							'followers': followers,
							'logout_url': users.create_logout_url(self.url_for("home")) }
			render_template(self,"follow_template.html", params)
    """

	@login_required
	def post(self):
		#Get the key of the user being followed. Update both users
		username = self.request.get('username')
		logging.info(username)
		(followee_key, followee) = get_user_data_by_user_name(username)

		if followee != None:
			#Found it
			user = users.get_current_user()
			(follower_key, follower) = get_user_data_by_user_id(user.user_id())
		
			if follower != None:
				if follower_key != followee_key:
					Follow.follow_user(self, follower, followee)
					#slow: delete stale data and repopulate on next access - add "follow" string extension
					#memcachemodule.cache_delete(follower_key)
		else:
			#invalid username specified.
			params = {	'logout_url': users.create_logout_url(self.url_for("home")) }
			
			self.session.add_flash("User with name " + username + " does not exist")

class Unfollow(webapp2.RequestHandler):

	def unfollow_user(self, follower, followee):
		#Helper method to unfollow
		#query = models.CattleFollower.gql("WHERE followee = :1 AND follower = :2", followee, follower)
		query = models.CattleFollower.all().filter('followee =', followee).filter('follower =', follower)
		cf = query.get()
		if cf != None:
			db.delete(cf)
			return True
		return False

	"""@login_required
	def get(self):
		user = users.get_current_user()
		(key, cattle) = get_user_data_by_user_id(user.user_id())
	
		if cattle != None:
			followers = []
			following = []
			for cf in cattle.following:
				following.append(cf.followee.user_name)
			for cf in cattle.followers:
				followers.append(cf.follower.user_name)
			params = {	'following': following,
							'followers': followers,
							'logout_url': users.create_logout_url(self.url_for("home")) }
			render_template(self,"unfollow_template.html", params)
	"""
	@login_required
	def post(self):
		#Get the key of the user being followed. Update both users
		username = self.request.get('username')
		(followee_key, followee) = get_user_data_by_user_name(username)

		if followee != None:
			#Found it
			user = users.get_current_user()
			(follower_key, follower) = get_user_data_by_user_id(user.user_id())
		
			if follower != None:
				if not Unfollow.unfollow_user(self, follower_key, followee_key):
					self.session.add_flash("Unfollow failed")
					
				self.redirect_to("unfollow")
		else:
			#invalid username specified.
			params = {	'logout_url': users.create_logout_url(self.url_for("home")) }
			
			self.session.add_flash("User with name " + username + " does not exist")
			self.redirect("unfollow_failed")

class QueryMatchingCattle(webapp2.RequestHandler):
	@login_required
	def post(self):
		partial_user_name= cgi.escape(self.request.get('term'))
		msg_query = db.GqlQuery("SELECT * FROM Cattle WHERE user_name >= :1 AND user_name < :2 ORDER BY user_name ASC", partial_user_name, unicode(partial_user_name) + u"\ufffd")
		cattle_list = msg_query.fetch(5)
		matching_cattle_list = [c.user_name for c in cattle_list]
		render_to_json(self, matching_cattle_list)
	
class QueryMatchingBrands(webapp2.RequestHandler):
	@login_required
	def post(self):
		partial_brand = cgi.escape(self.request.get('term'))
		msg_query = db.GqlQuery("SELECT * FROM Brand WHERE name >= :1 AND name < :2 ORDER BY name ASC", partial_brand, unicode(partial_brand) + u"\ufffd")
		brands_list = msg_query.fetch(5)
		matching_brands_list = [b.name for b in brands_list]
		render_to_json(self, matching_brands_list)

### /FOLLOW/UNFOLLOW PAGE CONTROLLERS ###

### MANAGEMENT CONTROLLERS ###
"""
Note to self - The google users documentation mentions that it is possible to have the case where
a user changes their email address and that is probably something that should be accounted
for. That is not being done here right now
"""
class AccountManager(webapp2.RequestHandler):
	def get(self):
		#Check if data for this user exists already
		user = users.get_current_user()
		if user != None:
			(key, cattle) = get_user_data_by_user_id(user.user_id())
			
			if cattle == None:
				#Create an account for the user
				#default_herd_key = self.app.config.get('default_herd_key')
				#cattle = models.Cattle(parent=default_herd_key, user_id=user.user_id(), user_name=user.nickname(), email=user.email())
				cattle = models.Cattle(user_id=user.user_id(), user_name=user.nickname(), email=user.email())
				cattle.put() 
				memcache.set(str(user.user_id()),cattle,time=120)
	
			#Get the URL to redir to. If there was nothing specified, redir to view_all page
			uri = self.request.uri
			if re.search(r"\?continue=", uri):
				uri = re.sub(r".*?continue=", "", uri)
			else:
				uri = webapp2.uri_for("teets_index")
	
			self.redirect(uri)

### /MANAGEMENT CONTROLLERS ###
