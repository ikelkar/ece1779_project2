from google.appengine.ext import db
from google.appengine.api import memcache;
import datetime
import time
import logging

### DB QUERY HELPERS ###

#Memcache
def get_user_data_by_user_id(user_id):
	cached = memcache.get(str(user_id))
	cattle = None
	key = None
	if cached != None:
		cattle = cached
		key = cached.key()
	else:
		user_query = db.GqlQuery("SELECT * FROM Cattle WHERE user_id = :1", user_id)
		cattle = user_query.get()
		if cattle != None:
			key = cattle.key()
			memcache.set(str(user_id),cattle,time=120)
	return (key, cattle)

#Memcache
def get_user_data_by_user_name(user_name):
	cached = memcache.get(str(user_name))
	key = None
	if cached != None:
		key = cached.key()
	else:
		user_query = db.GqlQuery("SELECT * FROM Cattle WHERE user_name = :1", user_name)
		cattle = user_query.get()
		if cattle != None:
			memcache.set(str(user_name),cattle,time=120)
			key = cattle.key()
			return (key, cattle)
		
	return (key, cached)

#Memcached
def get_brand_by_name(brand_name):
	cached = memcache.get(str(brand_name))
	key = None
	if cached != None:
		key = cached.key()
	else:
		brand_query = db.GqlQuery("SELECT * FROM Brand WHERE name = :1", brand_name)
		brand = brand_query.get()
		if brand != None:
			memcache.set(str(brand_name),brand,time=86400) #Brand can last in cache for a long time
			key = brand.key()
			return (key, brand)
		
	return (key, cached)

### /DB QUERY HELPERS ###

def get_batch_of_own_teets(cattle_key, order, timestamp):
	#logging.info(cattle_key)
	#cached = memcache.get(str(cattle_key))
	#logging.info(cached)
	#if cached != None:
	#	return cached
	#else:
	msg_query = None
	if timestamp != 0:
		#time = datetime.date.fromtimestamp(timestamp)
		filter_time = datetime.datetime.utcfromtimestamp(time.mktime(time.gmtime(float(timestamp))))
		if order == "ASC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE message_time > :1 AND cattle = :2 ORDER BY message_time ASC", filter_time, cattle_key)
		elif order == "DESC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE message_time < :1 AND cattle = :2 ORDER BY message_time DESC", filter_time, cattle_key)
	else:
		if order == "ASC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE cattle = :1 ORDER BY message_time ASC", cattle_key)
		elif order == "DESC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE cattle = :1 ORDER BY message_time DESC", cattle_key)
	
	teets = msg_query.fetch(5)
	#memcache.set(str(cattle_key),teets,time=120)
	return teets

#Memcached to get cattle
def get_batch_of_following_teets(cattle_key, order, timestamp):
	msg_query = None
	following = []	
	
	key = str(cattle_key) #+ "follow"
	#logging.info(key)
	cached = memcache.get(key)	
	#logging.info(cached)
	
	if cached != None:		
		for cf in cached.following:
			following.append(cf.followee.key())
	else:
		#If no cattle is found with the key then add key and cattle to cache
		memcache.set(key,db.get(cattle_key),time=120)
		for cf in db.get(cattle_key).following:
			following.append(cf.followee.key())

		
	if timestamp != 0:
		filter_time = datetime.datetime.utcfromtimestamp(time.mktime(time.gmtime(float(timestamp))))
		if order == "ASC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE message_time > :1 AND cattle IN :2 ORDER BY message_time ASC", filter_time, following)
		elif order == "DESC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE message_time < :1 AND cattle IN :2 ORDER BY message_time DESC", filter_time, following)
	else:
		if order == "ASC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE cattle IN :1 ORDER BY message_time ASC", following)
		elif order == "DESC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE cattle IN :1 ORDER BY message_time DESC", following)

	teets = msg_query.fetch(5)

	return teets

def get_batch_of_latest_teets(cattle_key, order, timestamp):
	msg_query = None

	if timestamp != 0:
		filter_time = datetime.datetime.utcfromtimestamp(time.mktime(time.gmtime(float(timestamp))))

		if order == "ASC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE message_time > :1 ORDER BY message_time ASC", filter_time, following)
		elif order == "DESC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE message_time < :1 ORDER BY message_time DESC", filter_time, following)
	else:
		if order == "ASC":
			msg_query = db.GqlQuery("SELECT * FROM Teet ORDER BY message_time ASC", following)
		elif order == "DESC":
			msg_query = db.GqlQuery("SELECT * FROM Teet ORDER BY message_time DESC", following)

	teets = msg_query.fetch(5)

	return teets

def get_batch_of_directed_teets(cattle_key, order, timestamp, number_teets_to_fetch):
	msg_query = None

	if timestamp != 0:
		filter_time = datetime.datetime.utcfromtimestamp(time.mktime(time.gmtime(float(timestamp))))

		if order == "ASC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE message_time > :1 AND user_refs = :2 ORDER BY message_time ASC", filter_time, cattle_key)
		elif order == "DESC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE message_time < :1 AND user_refs = :2 ORDER BY message_time DESC", filter_time, cattle_key)
	else:
		if order == "ASC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE user_refs = :1 ORDER BY message_time ASC", cattle_key)
		elif order == "DESC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE user_refs = :1 ORDER BY message_time DESC", cattle_key)

	teets = msg_query.fetch(number_teets_to_fetch)

	return teets


def get_upto_top_directed_teets(cattle_key, top_timestamp):
	msg_query = None

	top_filter_time = datetime.datetime.utcfromtimestamp(time.mktime(time.gmtime(float(top_timestamp))))
	print top_filter_time

	msg_query = db.GqlQuery("SELECT * FROM Teet WHERE message_time >= :1 AND user_refs = :2 ORDER BY message_time DESC", top_filter_time, cattle_key)
	teets = msg_query.fetch(None)

	return teets


def get_batch_brand_teets(brand_key, order, timestamp):
	msg_query = None

	if timestamp != 0:
		filter_time = datetime.datetime.utcfromtimestamp(time.mktime(time.gmtime(float(timestamp))))

		if order == "ASC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE message_time > :1 AND brands = :2 ORDER BY message_time ASC", filter_time, brand_key)
		elif order == "DESC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE message_time < :1 AND brands = :2 ORDER BY message_time DESC", filter_time, brand_key)
	else:
		if order == "ASC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE brands = :1 ORDER BY message_time ASC", brand_key)
		elif order == "DESC":
			msg_query = db.GqlQuery("SELECT * FROM Teet WHERE brands = :1 ORDER BY message_time DESC", brand_key)

	teets = msg_query.fetch(5)

	return teets
