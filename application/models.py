import webapp2
from google.appengine.ext import db

class Cattle(db.Model):
	"""Model for users and the relationships between them

	Each user has a list of Keys to other users that they are following.
	Each user also has a list of Keys that contains the users that are following them.
	"""

	#Data from Google User Service
	user_id = db.StringProperty(required=True)
	user_name = db.StringProperty(required=True)
	email = db.StringProperty(required=True)

	#Other metadata can go here
	join_date = db.DateTimeProperty(auto_now_add=True)
	teet_count = db.IntegerProperty(required=True, default=0)
	#location = 

	#Store any user prefs here
	user_info = db.StringProperty()
	image_data = db.BlobProperty()

	"""There should be some sort of entity added for a user avatar (image)
	Perhaps something like this:
	avatar = db.ReferenceProperty(CattleAvatarImage)
	
	class CattleAvatarImage(db.Model):
		image_data = db.BlobProperty()
		mime_type = db.StringProperty()
	"""

class CattleFollower(db.Model):
	"""Model for modeling the following/follower relationship

	Example usage:
	If f1 follows f2

	cf = CattleFollower(follower=f1, following=f2)

	#Cattle f1 is following:
	for cf in f1.following:
		username = cf.followee.user_name

	#f2's followers
	for cf in f2.followers:
		username = cf.follower.user_name
	"""
	follower = db.ReferenceProperty(Cattle, collection_name='following', required=True)
	followee = db.ReferenceProperty(Cattle, collection_name='followers', required=True)

class Brand(db.Model):
	"""Model for the brands (hashtags) 

	Teets contain keys that point to a Brand """
	name = db.StringProperty(required=True)

	@property
	def messages(self):
		return Teet.all().filter('brands', self.key())
	"""To retrieve all the messages that contain this brand:
	for c in brand.messages:
		#...
	"""

class Teet(db.Model):
	"""Model for each message
	A teet is a child of a Cattle entity """
	text = db.StringProperty(required=True)	#Actual message body
	message_time = db.DateTimeProperty(auto_now_add=True,required=True)	#Message date
	last_updated_date = db.DateProperty(auto_now=True,required=True) #Last edit date
	cattle = db.ReferenceProperty(Cattle, collection_name='teets', required=True)
	
	#Other metadata could go within this entity

	#Modeling of brands
	brands = db.ListProperty(db.Key)

	#User references (specified by %) - contains cattle Keys
	user_refs = db.ListProperty(db.Key)
