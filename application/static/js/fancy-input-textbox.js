var activeDynamicEntries = new Array();

function createDynamicEntry(element, text, type) {
	var count = activeDynamicEntries.length;
	activeDynamicEntries[count] = new Object();
	activeDynamicEntries[count].prev_text = text;
	activeDynamicEntries[count].active = false;
	activeDynamicEntries[count].element = element;
	activeDynamicEntries[count].type = type;
}

var searchActive = false;
var searchActiveElement = null;

function getActiveElementUnderCaret() {
	var target = null;
	if (window.getSelection) {
		target = window.getSelection().getRangeAt(0).commonAncestorContainer;
		return ((target.nodeType === 1) ? target : target.parentNode);
	} else if (document.selection) {
		var target = document.selection.createRange().parentElement();
	}
	return target;
}

function getCaretPosition(editableDiv) {
	var caretPos = 0, containerEl = null, sel, range;
	if (window.getSelection) {
		sel = window.getSelection();
		if (sel.rangeCount) {
			range = sel.getRangeAt(0);
			if (range.commonAncestorContainer.parentNode == editableDiv) {
				caretPos = range.endOffset;
			}
		}
	} else if (document.selection && document.selection.createRange) {
		range = document.selection.createRange();
		if (range.parentElement() == editableDiv) {
			var tempEl = document.createElement("span");
			editableDiv.insertBefore(tempEl, editableDiv.firstChild);
			var tempRange = range.duplicate();
			tempRange.moveToElementText(tempEl);
			tempRange.setEndPoint("EndToEnd", range);
			caretPos = tempRange.text.length;
		}
	}
	return caretPos;
}

function getCaretPositionNode(editableDiv) {
	var caretNode = 0, containerEl = null, sel, range;
	if (window.getSelection) {
		sel = window.getSelection();
		if (sel.rangeCount) {
			range = sel.getRangeAt(0);
			if (range.commonAncestorContainer.parentNode == editableDiv) {
				caretNode = sel.anchorNode;
			}
		}
	} else if (document.selection && document.selection.createRange) {
		range = document.selection.createRange();
		if (range.parentElement() == editableDiv) {
			var tempEl = document.createElement("span");
			editableDiv.insertBefore(tempEl, editableDiv.firstChild);
			var tempRange = range.duplicate();
			tempRange.moveToElementText(tempEl);
			tempRange.setEndPoint("EndToEnd", range);
			caretNode = null;

		}
	}
	return caretNode;
}

function getMatchingDynamicEntry(entry) {
	for ( var i = 0; i < activeDynamicEntries.length; i++) {
		if (activeDynamicEntries[i].element[0] == entry) {
			return activeDynamicEntries[i];
		}
	}

	return null;
}

function getLeftNode(elem) {
	var parentNode = elem.parentNode;
	for ( var i = 1; i < parentNode.childNodes.length; i++) {
		var child = parentNode.childNodes[i];
		if (child == elem) {
			return parentNode.childNodes[i - 1];
		}
	}

	return null;
}

function getRightNode(elem) {
	var parentNode = elem.parentNode;
	for ( var i = 0; i < parentNode.childNodes.length; i++) {
		var child = parentNode.childNodes[i];
		if (child == elem && i < parentNode.childNodes.length - 1) {
			return parentNode.childNodes[i + 1];
		}
	}

	return null;
}

function fixupDeadElements(current) {
	for ( var i = 0; i < activeDynamicEntries.length; i++) {
		var target = activeDynamicEntries[i];
		var new_text = target.element.text();
		if (target.prev_text != new_text) {

			// remove!
			if (new_text[0] != '@' && new_text[0] != '#') {

				var parentNode = target.element[0].parentNode;
				var rightNodeAgain = getRightNode(target.element[0]);

				if (current == target) {
					var caretPosition = getCaretPosition(target.element[0]);
					var leftString = new_text.substr(0, caretPosition);
					var rightString = new_text.substr(caretPosition);
					var newLeftNode = document.createTextNode(leftString);
					var newRightNode = document.createTextNode(rightString);

					parentNode.removeChild(target.element[0]);

					if (rightNodeAgain == null) {
						rightNodeAgain = parentNode.appendChild(newRightNode);

					} else {
						rightNodeAgain = parentNode.insertBefore(newRightNode, rightNodeAgain);
					}

					if (rightNodeAgain == null) {
						rightNodeAgain = parentNode.appendChild(newLeftNode);

					} else {
						rightNodeAgain = parentNode.insertBefore(newLeftNode, rightNodeAgain);
					}

					placeCaretAtEnd(newLeftNode);
				} else {
					var newNode = document.createTextNode(new_text);
					parentNode.removeChild(target.element[0]);

					if (rightNodeAgain == null) {
						rightNodeAgain = parentNode.appendChild(newNode);

					} else {
						rightNodeAgain = parentNode.insertBefore(newNode, rightNodeAgain);
					}

					var leftNode = getLeftNode(rightNodeAgain);
					if (leftNode != null) {
						placeCaretAtEnd(leftNode);
					} else {
						placeCaretAtEnd(rightNodeAgain);
					}
				}

				target.prev_text = new_text;
			}
		}
	}
}

function handleTeetChange() {
	var text = $('#new-teet-div').text();

	// $("#new-teet-div").find(".teet-dynamic-entry").remove();

	var active = getActiveElementUnderCaret();
	var target = getMatchingDynamicEntry(active);

	fixupDeadElements(target);

	if (target != null) {
		if (target.prev_text != target.element.text()) {
			var text = target.element.text();
			var caretPosition = getCaretPosition(target.element[0]);

			if (caretPosition == 0) {

				// Entry stopped being dynamic (user stripped '@'')
				if (text.length == 0 || (text[0] != "@" && text[0] != "#")) {
					var parentNode = target.element[0].parentNode;
					var newNode = document.createTextNode(text);
					var rightNodeAgain = getRightNode(target.element[0]);
					parentNode.removeChild(target.element[0]);

					if (rightNodeAgain == null) {
						rightNodeAgain = parentNode.appendChild(newNode);

					} else {
						rightNodeAgain = parentNode.insertBefore(newNode, rightNodeAgain);
					}

					placeCaretAtEnd(rightNodeAgain);
				}

				return;
			}

			var charAtCaret = text[caretPosition - 1];

			// OUCH, need to remove the character from the span, add to text
			// node
			// mmakowsk: DO NOT LIKE
			if (charAtCaret.charCodeAt(0) == 32 || charAtCaret.charCodeAt(0) == 160) {
				var count = text.length - caretPosition;
				target.element[0].innerText = text.substr(0, caretPosition - 1);

				// dont care anymore, just insert a new text node here
				var oneSpaceNode = document.createTextNode('\u00A0');
				var rightNodeAgain = getRightNode(target.element[0]);
				var parentNode = target.element[0].parentNode;
				if (rightNodeAgain == null) {
					rightNodeAgain = parentNode.appendChild(oneSpaceNode);

				} else {
					rightNodeAgain = parentNode.insertBefore(oneSpaceNode, rightNodeAgain);
				}

				placeCaretAtEnd(rightNodeAgain);

				return;
			}

			target.prev_text = target.element.text();
			searchActive = true;
			searchActiveElement = target;
			var mehText = target.element.text();
			$("#new-teet-div").autocomplete("search", mehText.substr(1));
			return target;
		}
	}

	// Try to find a new @ or # character (looking the main div)
	else {
		var offset = getCaretPosition(active);
		if (offset < 1) {
			return;
		}
		var textNode = getCaretPositionNode(active);
		var originalText = textNode.nodeValue;
		var targetCharacter = originalText[offset - 1];
		if (targetCharacter == "@" || targetCharacter == "#") {

			if (offset > 1) {
				var prevChar = originalText[offset - 2];
				if (prevChar.charCodeAt(0) != 32 && prevChar.charCodeAt(0) != 160) {
					return;
				}
			}
			var prefixEnd = offset - 1;
			var suffixStart = originalText.length;
			for ( var i = offset - 1; i < originalText.length; i++) {
				if (originalText[i] == ' ') {
					suffixStart = i;
					break;
				}
			}

			var prefix = originalText.substr(0, prefixEnd);
			var newSpan = originalText.substr(prefixEnd, suffixStart - prefixEnd);
			var suffix = originalText.substr(suffixStart, originalText.length - suffixStart);

			// Find the child node that's to the right of this node
			var parentNode = textNode.parentNode;
			var rightNode = getRightNode(textNode);

			var node = null;
			var caretNode = null;

			parentNode.removeChild(textNode);

			if (suffix.length > 0) {
				node = document.createTextNode(suffix);
				if (rightNode == null) {
					rightNode = parentNode.appendChild(node);

				} else {
					rightNode = parentNode.insertBefore(node, rightNode);
				}
			}

			node = document.createElement("span");
			node.className = "teet-dynamic-entry";
			node.innerHTML = newSpan;

			if (rightNode == null) {
				rightNode = parentNode.appendChild(node);

			} else {
				rightNode = parentNode.insertBefore(node, rightNode);
			}

			createDynamicEntry($(node), newSpan, targetCharacter);
			caretNode = rightNode;

			if (prefix.length > 0) {
				node = document.createTextNode(prefix);
				if (rightNode == null) {
					rightNode = parentNode.appendChild(node);

				} else {
					rightNode = parentNode.insertBefore(node, rightNode);
				}
			}

			placeCaretAtEnd(caretNode);
		}
	}

	return null;
}