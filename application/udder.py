import webapp2
from google.appengine.ext import db

import models
import handlers

import cgi

app = webapp2.WSGIApplication(routes = [
		webapp2.Route(r'/', handler=handlers.Home, name="home"),
											
		# maciej
		webapp2.Route(r'/style', handler=handlers.StyleShowHandler, name="style"),
		webapp2.Route(r'/jquery', handler=handlers.JquerySampleHandler, name="jquery"),
								
		# Public
		webapp2.Route(r'/login', handler=handlers.Login, name="login"),
		webapp2.Route(r'/logout', handler=handlers.Logout, name="logout"),
		
		# Search
		webapp2.Route(r'/search/user', handler=handlers.QueryMatchingCattle, name="search_user"),
		webapp2.Route(r'/search/brand', handler=handlers.QueryMatchingBrands, name="search_brand"),

		# Teets - Read
		webapp2.Route(r'/teets/frontpage', handler=handlers.TeetsFrontPageAjax, name="teets_front_page"),
		webapp2.Route(r'/teets/follow', handler=handlers.TeetsFollowAjax, name="teets_follow"),
		webapp2.Route(r'/teets/brands', handler=handlers.TeetsBrandsAjax, name="teets_brands"),
		webapp2.Route(r'/teets/gossip', handler=handlers.TeetsGossipAjax, name="teets_gossip"),
		webapp2.Route(r'/user/profile', handler=handlers.UserProfile, name="user_profile"),
		webapp2.Route(r'/user/saveprofile', handler=handlers.SaveUserProfile, name="user_profile"),
		# webapp2.Route(r'/teets/<teetid>', handler=TeetView, name="teet_view_by_id"),
		webapp2.Route(r'/teets/brand/<brand_name:\w+>', handler=handlers.TeetBrand, name="teet_brand"),
		# webapp2.Route(r'/teets/search', handler=TeetSearch, name="teet_search"),

		# Teets - Create, Edit
		webapp2.Route(r'/teets/new', handler=handlers.TeetsNew, name="teets_new"),
		webapp2.Route(r'/teets/edit/<teetid>', handler=handlers.TeetEdit, name="teet_edit"),

 		#User - Public
 		#webapp2.Route(r'/user/<username>', handler=User, name="user_teets"),
 		#webapp2.Route(r'/user/<username>/profile', handler=UserProfile, name="user_profile"),
 		#webapp2.Route(r'/user/<username>/followers', handler=, name="user_profile"),

 		#User - Private
 		#webapp2.Route(r'/settings', handler=UserSettings, name="user_settings")
		webapp2.Route(r'/follow', handler=handlers.Follow, name="follow"),
		webapp2.Route(r'/unfollow', handler=handlers.Unfollow, name="unfollow"),
		#webapp2.Route(r'/testing', handler=handlers.UserProfile, name="user_profile"),
		
		# Account Management
		webapp2.Route(r'/account_manager', handler=handlers.AccountManager, name="account_manager")
		],
		debug=True)
