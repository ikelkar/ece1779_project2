from google.appengine.ext import db
from google.appengine.api import memcache;
import datetime
import time

def cache_read(key):
    
    aeKey = str(key)
    cached = memcache.get(aeKey)
    if cached:
        return cached
   
    result = db.get(key)
    # since we missed, write back to the cache
    memcache.set(aeKey,result,300) #5min expiration time to avoid stale data (Could change this to optimize)
    return result


#Note: There is no way to update both the datastore and the memcache in a single transaction
#I.e. No way to avoid the possibility that the cache may contain old data
def cache_write(key, value):
    
    aeKey = str(key)
    db.put(value)
    cached = memcache.get(aeKey)
    if cached:
        #Delete the key to avoid confict of two processes updating an entity
        #Memcache will have the value of whichever update occurs last
        #Somewhat better to just delete the key when the datstore changes
        #and let the next read attempt populate the cache with a current value
        memcache.delete(aeKey)        
        #memcache.replace(key,value)
        
    memcache.add(aeKey,value,300)

#Force Delete
def cache_delete(key):
    memcache.delete(str(key))
    
    


    
    